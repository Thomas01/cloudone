import React, { useState } from 'react';

const AddPrinterForm = props => {
	const initialFormState = { id: null, name: '', printerIp: '' }
	const [ printer, setPrinter ] = useState(initialFormState)

	const handleInputChange = event => {
		const { name, value } = event.target

		setPrinter({ ...printer, [name]: value })
	}

	return (
		<form
			onSubmit={event => {
				event.preventDefault()
				if (!printer.name || !printer.printerIp) return

				props.addPrinter(printer)
				setPrinter(initialFormState)
			}}
		>
			<label>Name:</label>
			<input type="text" name="name" value={printer.name} onChange={handleInputChange} />
			<label>IP Address:</label>
			<input type="text" name="printerIp" value={printer.printerIp} onChange={handleInputChange} />
			<button>Add New Printer</button>
		</form>
	)
}

export default AddPrinterForm;
