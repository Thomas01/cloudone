import React, { useState, useEffect } from 'react';

const EditPrinterForm = props => {
  const [printer, setPrinter] = useState(props.currentPrinter)

  const handleInputChange = event => {
    const { name, value } = event.target

    setPrinter({ ...printer, [name]: value })
  }

  useEffect(() => {
  setPrinter(props.currentPrinter)
}, [props])

  return (
    <form
      onSubmit={event => {
        event.preventDefault()

        props.updatePrinter(printer.id, printer)
      }}
    >
      <label>Name:</label>
      <input type="text" name="name" value={printer.name} onChange={handleInputChange} />
      <label>IP Address:</label>
      <input type="text" name="username" value={printer.printerIp} onChange={handleInputChange} />
      <button>Update Printer</button>
      <button onClick={() => props.setEditing(false)} className="button muted-button">
        Cancel
      </button>
    </form>
  )
}

export default EditPrinterForm;