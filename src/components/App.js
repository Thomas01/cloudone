import React, { useState, Fragment }from 'react';

import PrinterTable from './PrinterTable';
import AddPrinterForm from './forms/AddPrinterForm';
import EditPrinterForm from './forms/EditPrinterForm';

const App = () => {
	// Data
	const printersData = [
		{ id: 1, name: 'Canon Pixma', printerIp: '5482 103.28.37.178' },
		{ id: 2, name: 'Epson', printerIp: '5356 66.249.78.168' },
		{ id: 3, name: 'Brothers', printerIp: '5356 66.249.78.169' },
		{ id: 4, name: 'Epson', printerIp: '5356 66.249.78.166' },
		{ id: 5, name: 'Brother', printerIp: '5356 66.249.78.161' },
	]

	const initialFormState = { id: null, name: '', printerIp: '' }

	// Setting state
	const [ printers, setPrinters ] = useState(printersData);
	const [ currentPrinter, setCurrentPrinter ] = useState(initialFormState);
	const [ editing, setEditing ] = useState(false);

	// CRUD operations
	const addPrinter = printer => {
		printer.id = printers.length + 1
		setPrinters([ ...printers, printer])
	}

	const deletePrinter = id => {
		setEditing(false)

		setPrinters(printers.filter(printer => printer.id !== id))
	}

	const updatePrinter = (id, updatedPrinter) => {
		setEditing(false)

		setPrinters(printers.map(printer => (printer.id === id ? updatedPrinter : printer)))
	}

	const editRow = printer => {
		setEditing(true)

		setCurrentPrinter({ id: printer.id, name: printer.name, printerIp: printer.printerIp })
	}

	return (
		<div className="container">
			<div class="jumbotron">
			<h2>Admin Dashboard</h2>
			<p>Focusing Technologies</p>
			</div>
			<div className="flex-row">
				<div className="flex-large">
					{editing ? (
						<Fragment>
							<h2>Edit Printer Model</h2>
							<EditPrinterForm
								editing={editing}
								setEditing={setEditing}
								currentPrinter={currentPrinter}
								updatePrinter={updatePrinter}
							/>
						</Fragment>
					) : (
						<Fragment>
							<h2>Add Printer</h2>
							<AddPrinterForm addPrinter={addPrinter} />
						</Fragment>
					)}
				</div>
				<div className="flex-large">
					<h2>View Printer</h2>
					<PrinterTable printers={printers} editRow={editRow} deletePrinter={deletePrinter} />
				</div>
			</div>
			<footer class="jumbotron text-center">

				<p>© 2019 Copyright</p>

			</footer>
		</div>
	)
}

export default App;
