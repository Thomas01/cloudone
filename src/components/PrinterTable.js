import React from 'react';

const PrinterTable = props => (
  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>IP Address</th>
        <th>Status</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {props.printers.length > 0 ? (
        props.printers.map(printer => (
          <tr key={printer.id}>
            <td>{printer.name}</td>
            <td>{printer.printerIp}</td>
            <td>
              <button>
                Active
              </button>
            </td>
            <td>
              <button
                onClick={() => {
                  props.editRow(printer)
                }}
                className="button muted-button"
              >
                Edit
              </button>
              <button
                onClick={() => props.deletePrinter(printer.id)}
                className="button muted-button"
              >
                Delete
              </button>
            </td>
          </tr>
        ))
      ) : (
        <tr>
          <td colSpan={3}>Add a printer</td>
        </tr>
      )}
    </tbody>
  </table>
)

export default PrinterTable;
